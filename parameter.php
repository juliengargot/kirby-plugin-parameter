<?php

/**
 *
 * Parameter
 *
 * A set of kirby params helpers.
 *
 * @author    Julien Gargot <julien@g-u-i.net>
 * @link      http://g-u-i.net
 * @copyright Julien Gargot
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
class Parameter {

  /**
   * Default options for tag methods
   *
   * @var array
   */
  public static $defaults = array(
    'key' => 'tag',
    'separator' => ',',
  );

  /**
   * Test if a given tag is present as a parameter in an URL.
   *
   *
   * @param  string   $tag The tag to search
   * @param  string   $url The url to test
   * @param  string   $key The parameter key
   * @return boolean  The result.
   */
  public static function isActive($tag, $key = null, $url = null, $separator = null) {
    $url = $url ?: url::current();
    $key = $key ?: static::$defaults['key'];
    $separator = $separator ?: static::$defaults['separator'];

    $param = static::param($key, $url);
    $values = str::split($param, $separator);
    return in_array($tag, $values);
  }

  /**
   * Get a parameter from the given URL
   *
   * @param   string   $url The url to test
   * @param   string   $key The key to look for. Pass false or null to return the entire params array.
   * @param   mixed    $default Optional default value, which should be returned if no element has been found
   * @return  mixed
   */
  public static function param($key = null, $url = null, $default = null) {
    $url = $url ?: url::current();
    $key = $key ?: static::$defaults['key'];
    $params = url::params($url);
    return a::get($params, $key, $default);
  }

  /**
   * Add a value to a parameter on the given URL
   *
   * @param   string
   * @param   string
   * @param   string
   * @return  string
   */
  public static function add($tag, $key = null, $url = null, $separator = null) {
    $url = $url ?: url::current();
    $key = $key ?: static::$defaults['key'];
    $separator = $separator ?: static::$defaults['separator'];

    if ( static::isActive($tag, $key, $url, $separator) ) {
      return $url;
    }

    $params = a::update(url::params($url), [
      $key => function($value) use ($tag,$separator) {
        $value = $tag .$separator. $value;
        return preg_replace('/\,$/', '', $value); // strip comma
      }
    ]);

    return url::build(array('params' => $params), $url);
  }

  /**
   * Remove a value from a parameter on the given URL
   *
   * @param   string
   * @param   string
   * @param   string
   * @return  string
   */
  public static function remove($tag, $key = null, $url = null, $separator = null) {
    $url = $url ?: url::current();
    $key = $key ?: static::$defaults['key'];
    $separator = $separator ?: static::$defaults['separator'];

    if ( !static::isActive($tag, $key, $url, $separator) ) {
      return $url;
    }

    $params = array_filter(a::update(url::params($url), [
      $key => function($value) use ($tag,$separator) {
        $value = preg_replace("/$tag($separator?)/", '', $value);
        return preg_replace('/\,$/', '', $value); // strip comma
      }
    ]));

    return url::build(array('params' => $params), $url);
  }
}
